//
//  WhaterRemoteRepository.swift
//  VpTech Wather
//
//  Created by Kruding.com on 22/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//
import Foundation

class WhaterRemoteRepository: WheatherRepository {
    func getForeCastForCity(cityName: String, completion: @escaping ([Forecast]?, String?) -> Void) {
        guard let url = URL(string: "\(Constants.API_URL.rawValue)?q=\(cityName)&appid=\(Constants.APP_ID.rawValue)&units=metric") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    // let data2 =  UserDefaults.standard.value(forKey: "data") as! Data
                    //     UserDefaults.standard.set(data2, forKey: "data")
                    let forecastResponse = try decoder.decode(ForecastResponse.self, from: data)
                    
                    completion(forecastResponse.forecastList,nil)
                }
                catch let jsonError
                {
                    print(jsonError.localizedDescription)
                }
                
            }
        }.resume()
    }
    
    
}
