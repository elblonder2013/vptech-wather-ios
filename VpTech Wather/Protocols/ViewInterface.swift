//
//  ViewInterface.swift
//  VpTech Wather
//
//  Created by Developer on 13/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

// MARK: - View Interface
/// The protocol that define for all ViewController need to conform
protocol ViewInterface: class { }

// MARK: - View Interface Helpers
/// Extra capabilities for ViewInterface
extension ViewInterface { }
