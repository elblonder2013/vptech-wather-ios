//
//  InteractorInterface.swift
//  VpTech Wather
//
//  Created by Developer on 13/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

// MARK: - Interactor Interface
/// The protocol that define for all Interactor need to conform
protocol InteractorInterface: class { }

// MARK: - Interactor Interface Helper
/// Extra capabilities for InteractorInterface
extension InteractorInterface { }

