//
//  String+Additions.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//

extension String{
    func substring( from startIndex:Int, to endIndex:Int)->String{
        return String(self[self.index(self.startIndex, offsetBy: startIndex)..<self.index(self.startIndex, offsetBy: endIndex)])
    }
}
