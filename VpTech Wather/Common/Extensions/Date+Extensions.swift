//
//  Date+Extensions.swift
//  VpTech Wather
//
//  Created by Kruding.com on 25/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//
import Foundation

extension Date {
    static func dateFromStringFormat( format:String, stringDate:String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: stringDate)
        return date!
    }
    
    func stringFromDate(format:String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
