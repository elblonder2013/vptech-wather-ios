//
//  Constants.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//

enum Constants:String{
    case WEB_URL = "https://openweathermap.org/img/wn/"
    case API_URL = "https://api.openweathermap.org/data/2.5/forecast";
    case APP_ID = "8be9cb2b3d97ef7033357a91628f5884"
}
