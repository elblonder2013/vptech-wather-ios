//
//  ForecastDetailPresenter.swift
//  VpTech Wather
//
//  Created by alexei on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

final class ForecastDetailPresenter {
    
    private let _wireframe: ForecastDetailWireframeInterface
    private unowned let _view: ForecastDetailViewInterface
    private let _interactor: ForecastDetailInteractorInterface
    
    init(wireframe: ForecastDetailWireframeInterface, view: ForecastDetailViewInterface, interactor: ForecastDetailInteractorInterface) {
        self._wireframe = wireframe
        self._view = view
        self._interactor = interactor
    }
  
    var presentation: ForecastDetailPresentation? {
        didSet{
            if let presentation = presentation {
                hourlyForecastList = presentation.forecastList.map { TempCellPresentation(forecast: $0) }
            }
            _view.reloadView()
        }
    }
    
    var hourlyForecastList: [TempCellPresentation] = []
    
    var numberOfHourlyForecast: Int {
        return hourlyForecastList.count
    }
    
    func hourlyForecatPresentationForIndex(_ index: Int) -> TempCellPresentation {
        return hourlyForecastList[index]
    }
}

extension ForecastDetailPresenter: ForecastDetailPresenterInterface {
    func viewDidLoad() {
        
    }
}
