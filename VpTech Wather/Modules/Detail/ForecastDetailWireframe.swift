//
//  ForecastDetailWireframe.swift
//  VpTech Wather
//
//  Created by alexei on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

final class ForecastDetailWireframe: BaseWireframe {
    init(_ presentation: ForecastDetailPresentation) {
        let moduleViewController = DetailViewController(nibName: "DetailViewController", bundle: nil)
        super.init(viewController: moduleViewController)
        let interactor = ForecastDetailInteractor()
        let presenter = ForecastDetailPresenter(wireframe: self, view: moduleViewController, interactor: interactor)
        presenter.presentation = presentation
        moduleViewController.presenter = presenter
    }
}

extension ForecastDetailWireframe: ForecastDetailWireframeInterface {

}
