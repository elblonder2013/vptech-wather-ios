//
//  MovieDetailInterface.swift
//  VpTech Wather
//
//  Created by alexei on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

protocol ForecastDetailPresenterInterface: PresenterInterface {
    var presentation: ForecastDetailPresentation?  { get }
    var numberOfHourlyForecast:Int { get }
    func hourlyForecatPresentationForIndex(_ index:Int)->TempCellPresentation
}

protocol ForecastDetailViewInterface: ViewInterface {
    func reloadView()
}

protocol ForecastDetailInteractorInterface: InteractorInterface {
    
}

protocol ForecastDetailWireframeInterface: WireframeInterface {
    
}
