//
//  ForecastPresentation.swift
//  VpTech Wather
//
//  Created by Developer on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//


protocol ForecastPresentation {
    var minTemp:String { get }
    var maxTemp:String { get }
}
