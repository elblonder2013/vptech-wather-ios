//
//  TempCellPresentation.swift
//  VpTech Wather
//
//  Created by alexei on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

import Foundation

class TempCellPresentation {
    var forecast:Forecast
    var hour = ""
    var temp = ""
    var iconUrl = ""
    
    init(forecast:Forecast) {
        self.forecast = forecast
        //output setup
        if let firstWeahter = forecast.weatherList.first{
            iconUrl =  Constants.WEB_URL.rawValue + "\(firstWeahter.icon)@2x.png"
        }
        let date = Date.dateFromStringFormat(format: "yyyy-MM-dd HH:mm:ss", stringDate: forecast.dateString)
        hour = date.stringFromDate(format: "HH")
        temp = "\(Int(forecast.mainForecast.tempMax))°"
    }
}
