//
//  TempCell.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//

import UIKit
import SDWebImage
class TempCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: TempCellPresentation? {
        didSet {
            guard let item = item else {
                return
            }
            hourLabel.text = item.hour  + ":00"
            tempLabel.text = item.temp
            iconImageView.sd_setImage(with: URL(string: item.iconUrl), completed: nil)
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
    }
    
}
