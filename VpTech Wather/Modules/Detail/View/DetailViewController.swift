//
//  DetailViewController.swift
//  VpTech Wather
//
//  Created by alexei on 15/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var presenter: ForecastDetailPresenterInterface!
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weekDayLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
   
    @IBOutlet weak var hoursCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        guard let presentation = presenter.presentation else { return }
        setupViews(presentation)
        setupCollectionView()
    }
    
    func setupViews(_ presentation:ForecastDetailPresentation){
        maxLabel.text = presentation.maxTemp
        minLabel.text = presentation.minTemp
        cityLabel.text = presentation.city
        weekDayLabel.text = presentation.weekDay
    }
    
    func setupCollectionView(){
        hoursCollectionView.register(TempCell.nib, forCellWithReuseIdentifier: TempCell.identifier)
        hoursCollectionView.dataSource = self
    }
}
extension DetailViewController: ForecastDetailViewInterface, UICollectionViewDelegate, UICollectionViewDataSource {
    func reloadView() {
        DispatchQueue.main.async {
            self.hoursCollectionView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfHourlyForecast
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TempCell.identifier, for: indexPath) as? TempCell, indexPath.row < presenter.numberOfHourlyForecast  else {return UICollectionViewCell()}
        cell.item = presenter.hourlyForecatPresentationForIndex(indexPath.row)
        return cell
    }
}
