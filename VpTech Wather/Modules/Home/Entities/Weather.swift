//
//  Weather.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//



struct Weather:Codable {
    var weatherId:Int
    var main:String
    var weatherDescription:String
    var icon:String
    
    private enum CodingKeys: String, CodingKey {
        case weatherId = "id"
        case main = "main"
        case weatherDescription = "description"
        case icon = "icon"

    }
}
