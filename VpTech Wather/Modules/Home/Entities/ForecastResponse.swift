//
//  ForecastResponse.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//
import Foundation
struct ForecastResponse:Codable {
    var cod:String
    var forecastList:[Forecast]
    
    private enum CodingKeys: String, CodingKey {
        case cod = "cod"
        case forecastList = "list"
    }
}
