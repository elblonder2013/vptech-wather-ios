//
//  Wind.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//


struct Wind:Codable {
    var speed:Double
    var deg:Int
}
