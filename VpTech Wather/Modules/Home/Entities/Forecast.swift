//
//  DayForecast.swift
//  VpTech Wather
//
//  Created by Kruding.com on 22/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//


struct Forecast:Codable {
    
    var timeSpam:Int
    var dateString:String
    var mainForecast:MainForecast
    var weatherList:[Weather]
    var wind:Wind
    
    private enum CodingKeys: String, CodingKey {
        case timeSpam = "dt"
        case dateString = "dt_txt"
        case mainForecast = "main"
        case weatherList = "weather"
        case wind = "wind"
    }
}
