//
//  MainForecast.swift
//  VpTech Wather
//
//  Created by Kruding.com on 23/12/2020.
//  Copyright © 2020 Alexei Pineda. All rights reserved.
//


struct MainForecast:Codable {
    var tempMin:Double
    var tempMax:Double
    var pressure:Int
    var seaLevel:Int
    var grndLevel:Int
    var humidity:Int
   
    private enum CodingKeys: String, CodingKey {
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure = "pressure"
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity = "humidity"
    }
}
