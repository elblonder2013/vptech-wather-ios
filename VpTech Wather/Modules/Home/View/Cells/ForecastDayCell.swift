//
//  ForecastDayCell.swift
//  VpTech Wather
//
//  Created by alexei on 14/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

import UIKit

class ForecastDayCell: UITableViewCell {

    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var presentation: ForecastCellPresentacion? {
        didSet {
            guard let presentation = presentation else {
                return
            }
            dayLabel.text = presentation.weekDay
            minTempLabel.text = presentation.minTemp
            maxTempLabel.text = presentation.maxTemp
            //pictureImageView?.image = UIImage(named: item.pictureUrl)
        }
    }
}
