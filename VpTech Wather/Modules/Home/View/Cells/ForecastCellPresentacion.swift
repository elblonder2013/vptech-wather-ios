//
//  ForecastCellPresentacion.swift
//  VpTech Wather
//
//  Created by alexei on 14/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

import UIKit

class ForecastCellPresentacion:ForecastPresentation {
  
    var minTemp = ""
    var maxTemp = ""
    var dateString:String
    var weekDay:String
    
    init(forecasts:[Forecast], dateString:String) {
      
        self.dateString = dateString
        let date = Date.dateFromStringFormat(format: "yyyy-MM-dd", stringDate: dateString)
        self.weekDay = date.stringFromDate(format: "E dd/MM/yyyy")
        
        if let firstForecast = forecasts.first{
            var minTemp = firstForecast.mainForecast.tempMin
            var maxTemp = firstForecast.mainForecast.tempMin
            for forecast in forecasts {
                if forecast.mainForecast.tempMin < minTemp {
                    minTemp = forecast.mainForecast.tempMin
                }
                if forecast.mainForecast.tempMax > maxTemp {
                    maxTemp = forecast.mainForecast.tempMax
                }
            }
            self.minTemp = "\(Int(minTemp))°"
            self.maxTemp = "\(Int(maxTemp))°"
        }
    }

}
