//
//  HomeViewController.swift
//  VpTech Wather
//
//  Created by alexei on 14/1/21.
//  Copyright © 2021 Alexei Pineda. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    var presenter: HomePresenterInterface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        _setupTableView()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        navigationItem.title = "\(presenter.city ) Forecast"
        LoadingView.show()
        let errorView = ErrorView()
        view.addSubview(errorView)
        errorView.addConstraint(attribute: .centerX, alignElement: self.tableView, alignElementAttribute: .centerX, constant: 0)
        errorView.addConstraint(attribute: .centerY, alignElement:  self.tableView, alignElementAttribute: .centerY, constant: 0)
        errorView.isHidden = true
        
    }
    
    func _setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.backgroundView = UIView()
        tableView.allowsSelection = true
        tableView.tableFooterView = UIView()
        tableView.register(ForecastDayCell.nib, forCellReuseIdentifier: ForecastDayCell.identifier)
    }
}

extension HomeViewController: HomeViewInterface, UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfForecast
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ForecastDayCell.identifier) as? ForecastDayCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets.zero
        cell.presentation = presenter.forecastForIndex(indexPath.row)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didPressNavigateToDetailAtIndex(index: indexPath.row)
    }
    
    func reloadView() {
        DispatchQueue.main.async {
            LoadingView.hide()
            self.tableView.reloadData()
        }
    }
}
