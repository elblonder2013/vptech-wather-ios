//
//  HomeInterface.swift
//  VpTech Wather
//
//  Created by Kruding.com on 12/1/21.
//  Copyright © 2021 Iflet.tech. All rights reserved.
//

import Foundation

enum HomeNavigationOption {
    case forecastDetail(_ presentation: ForecastDetailPresentation)
}

protocol HomeWireframeInterface: WireframeInterface {
    func navigate(to option: HomeNavigationOption)
}

protocol HomePresenterInterface: PresenterInterface {
    var city:String { get }
    var numberOfForecast:Int { get }
    func bindForecastList(_ list:[Forecast])
    func forecastForIndex(_ index:Int)->ForecastCellPresentacion
    func didPressNavigateToDetailAtIndex(index:Int)
    
}

protocol HomeViewInterface: ViewInterface {
    func reloadView()
}

protocol HomeInteractorInterface: InteractorInterface {
    func fetchForecast(completionHandler: @escaping ([Forecast]?,_ error:String?) -> Void)
    func fetchOfflineForecast(completionHandler: @escaping ([Forecast]?,_ error:String?) -> Void)
    func persisForecasts(_ forecasts:[Forecast],completionHandler: @escaping () -> Void)
}
