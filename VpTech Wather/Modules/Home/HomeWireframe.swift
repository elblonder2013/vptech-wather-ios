//
//  HomeWireframe.swift
//  VpTech Wather
//
//  Created by Kruding.com on 12/1/21.
//  Copyright © 2021 Iflet.tech. All rights reserved.
//

import UIKit

class HomeWireframe: BaseWireframe {
    init() {
        let moduleViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        super.init(viewController: moduleViewController)
        let interactor = HomeInteractor()
        let presenter = HomePresenter(wireframe: self, view: moduleViewController, interactor: interactor)
        moduleViewController.presenter = presenter
    }
    
    private func _navigateToForecastDetail(_ presentation: ForecastDetailPresentation) {
        navigationController?.pushWireframe(ForecastDetailWireframe(presentation))
    }
}


extension HomeWireframe:HomeWireframeInterface{
    func navigate(to option: HomeNavigationOption) {
        switch option {
        case .forecastDetail(let presentation):
            _navigateToForecastDetail(presentation)
        }
    }
}
