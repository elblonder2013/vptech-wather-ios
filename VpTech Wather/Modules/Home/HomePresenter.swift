//
//  HomePresenter.swift
//  VpTech Wather
//
//  Created by Kruding.com on 12/1/21.
//  Copyright © 2021 Iflet.tech. All rights reserved.
//


final class HomePresenter {
    private let _wireframe: HomeWireframeInterface
    private unowned let _view: HomeViewInterface
    private let _interactor: HomeInteractorInterface
    
    var city = "Paris"
    var forecastStore:[String:[Forecast]] = [:]
    var forecastList: [ForecastCellPresentacion] = [] {
        didSet{
            _view.reloadView()
        }
    }
    
    func bindForecastList(_ list:[Forecast]){
        forecastStore = [String:[Forecast]]()
        for forecast in list{
            let day = String(forecast.dateString.split(separator: " ")[0])
            if forecastStore[day] != nil {
                forecastStore[day]?.append(forecast)
            } else{
                forecastStore[day] = [forecast]
            }
        }
       
        let keys = forecastStore.keys.sorted()
        var forecastList = [ForecastCellPresentacion]()
        for key in keys{
            forecastList.append(ForecastCellPresentacion(forecasts: forecastStore[key]!, dateString: key))
        }
        self.forecastList = forecastList
    }
    
    var numberOfForecast:Int{
        return forecastList.count
    }
    
    func forecastForIndex(_ index:Int)->ForecastCellPresentacion{
        return forecastList[index]
    }
   
    init(wireframe: HomeWireframeInterface, view: HomeViewInterface, interactor: HomeInteractorInterface) {
        _view = view
        _wireframe = wireframe
        _interactor = interactor
    }
}

extension HomePresenter: HomePresenterInterface {
    
    func didPressNavigateToDetailAtIndex(index:Int){
        let dayString = self.forecastList[index].dateString
        guard let forecasts = forecastStore[dayString] else {
            return
        }
        let forecastDetailPresentation = ForecastDetailPresentation(forecasts: forecasts, dateString: dayString, city: city)
        _wireframe.navigate(to: .forecastDetail(forecastDetailPresentation))
       
    }
    
    func viewDidLoad() {
        _interactor.fetchForecast { (forecasts, errorMessage) in
            if let forecasts = forecasts {
                self.bindForecastList(forecasts)
                self._interactor.persisForecasts(forecasts) {   }
            } else if errorMessage != nil {
                //if ocurred and error from api, trying to get forecast saved
                self._interactor.fetchOfflineForecast { (forecasts, errorMessage) in
                    if let forecasts = forecasts {
                        self.bindForecastList(forecasts)
                    } else {
                        //notify to user the error
                    }
                }
            }
        }
        
    }
    
}
