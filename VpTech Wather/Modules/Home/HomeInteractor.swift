//
//  HomeInteractor.swift
//  VpTech Wather
//
//  Created by Kruding.com on 12/1/21.
//  Copyright © 2021 Iflet.tech. All rights reserved.
//

import Foundation

class HomeInteractor {
    func _fetchForecast(completionHandler: @escaping ([Forecast]?, String?) -> Void) {
      
        guard let url = URL(string: "\(Constants.API_URL.rawValue)?q=Paris&appid=\(Constants.APP_ID.rawValue)&units=metric") else { return print("Error fetching API, no given URL within the function")}
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            if let error = error{
                print("Error with fetching forecast: \(error)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                print("Error with the response, unexpected result. \n response: \(String(describing: response))\n error : \(String(describing: error))")
                return
            }
            if let data = data,
               let forecastResponse = try? JSONDecoder().decode(ForecastResponse.self, from: data) {
                completionHandler(forecastResponse.forecastList, nil)
            }
            
        })
        task.resume()
    }
    
    func _fetchOfflineForecast(completionHandler: @escaping ([Forecast]?, String?) -> Void) {
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            //completionHandler("Could not retrieve documents directory")
            return
        }
        let url = documentsUrl.appendingPathComponent("forecasts.json")
            let decoder = JSONDecoder()
            do {
              
                let data = try Data(contentsOf: url, options: [])
                let forecasts = try decoder.decode([Forecast].self, from: data)
                completionHandler(forecasts, nil)
            } catch {
                fatalError(error.localizedDescription)
            }
    }
    
    func _persisForecasts(_ forecasts:[Forecast],completionHandler: @escaping () -> Void){
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            completionHandler()
            return
        }
        let url = documentsUrl.appendingPathComponent("forecasts.json")
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(forecasts)
            // 3. Write this data to the url specified in step 1
            try data.write(to: url, options: [])
        } catch {
            completionHandler()
        }
    }
    
}
extension HomeInteractor: HomeInteractorInterface {
    func fetchForecast(completionHandler: @escaping ([Forecast]?, String?) -> Void) {
        _fetchForecast(completionHandler: completionHandler)
    }
    
    func fetchOfflineForecast(completionHandler: @escaping ([Forecast]?, String?) -> Void) {
        _fetchOfflineForecast(completionHandler: completionHandler)
    }
    
    func persisForecasts(_ forecasts:[Forecast],completionHandler: @escaping () -> Void){
        _persisForecasts(forecasts) {
            completionHandler()
        }
    }
}
